# Fractals (javafx app)

Maven + fractals package

## Requirements

java version 17+, maven.

## Start

To run app, use:

```mvn clean javafx:run```

---

To package this as usual application, use:

```mvn clean javafx:jlink```

This creates target/fractals directory with application.
