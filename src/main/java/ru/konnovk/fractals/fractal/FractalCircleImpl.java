package ru.konnovk.fractals.fractal;

public class FractalCircleImpl implements Fractal {

    @Override
    public double evaluate(double x, double y) {
        if (x * x + y * y <= 4) {
            return 1 - ((x * x + y * y) / 4.);
        }
        return 0;
    }
}
